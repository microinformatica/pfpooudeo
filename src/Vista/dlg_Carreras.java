/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package Vista;

import carreras.carreras;
import carreras.dbCarreras;
import javax.swing.JOptionPane;

/**
 *
 * @author bmfil
 */
public class dlg_Carreras extends javax.swing.JDialog {

    /**
     * Creates new form Insertar_libros
     */
    public dlg_Carreras(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        txtNumSem = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtNumMat = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        btnCancelarLi = new javax.swing.JButton();
        btnVolverALi = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        btnAgregarLi1 = new javax.swing.JButton();
        btnBuscarli = new javax.swing.JButton();
        btnLimpiarli = new javax.swing.JButton();
        btnNuevoli = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        txtClave = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(386, 560));
        getContentPane().setLayout(null);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 160, 76, 20);

        txtNumSem.setEnabled(false);
        getContentPane().add(txtNumSem);
        txtNumSem.setBounds(210, 200, 140, 22);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel6.setText("Numero Semestres");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(30, 200, 170, 21);

        txtNumMat.setEnabled(false);
        getContentPane().add(txtNumMat);
        txtNumMat.setBounds(210, 240, 112, 22);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel7.setText("Num. Materias");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(30, 240, 140, 21);

        btnCancelarLi.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnCancelarLi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/prohibicion.png"))); // NOI18N
        btnCancelarLi.setText("Cancelar");
        btnCancelarLi.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(102, 0, 102)));
        btnCancelarLi.setEnabled(false);
        btnCancelarLi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarLiActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelarLi);
        btnCancelarLi.setBounds(140, 330, 90, 30);

        btnVolverALi.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnVolverALi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/girar-a-la-izquierda.png"))); // NOI18N
        btnVolverALi.setText("Volver");
        btnVolverALi.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(102, 0, 102)));
        btnVolverALi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverALiActionPerformed(evt);
            }
        });
        getContentPane().add(btnVolverALi);
        btnVolverALi.setBounds(250, 330, 90, 30);

        jPanel1.setBackground(new java.awt.Color(255, 255, 51));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLabel1.setText("       ¬¬¬ Carreras ¬¬¬");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(50, 10, 320, 32);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 400, 50);

        jPanel2.setBackground(new java.awt.Color(255, 255, 102));

        jPanel4.setBackground(new java.awt.Color(255, 255, 102));
        jPanel2.add(jPanel4);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(350, 50, 30, 490);

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel10.setText("Clave Carrera");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(30, 70, 110, 20);

        txtNombre.setEnabled(false);
        getContentPane().add(txtNombre);
        txtNombre.setBounds(130, 160, 220, 22);

        btnAgregarLi1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnAgregarLi1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/libro (1).png"))); // NOI18N
        btnAgregarLi1.setText("Agregar");
        btnAgregarLi1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(102, 0, 102)));
        btnAgregarLi1.setEnabled(false);
        btnAgregarLi1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarLi1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnAgregarLi1);
        btnAgregarLi1.setBounds(260, 110, 80, 30);

        btnBuscarli.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnBuscarli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/busqueda.png"))); // NOI18N
        btnBuscarli.setText("Buscar");
        btnBuscarli.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(102, 0, 102)));
        btnBuscarli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarliActionPerformed(evt);
            }
        });
        getContentPane().add(btnBuscarli);
        btnBuscarli.setBounds(150, 90, 85, 30);

        btnLimpiarli.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnLimpiarli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/escoba.png"))); // NOI18N
        btnLimpiarli.setText("limpiar");
        btnLimpiarli.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(102, 0, 102)));
        btnLimpiarli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarliActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiarli);
        btnLimpiarli.setBounds(40, 330, 90, 26);

        btnNuevoli.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        btnNuevoli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/agregar.png"))); // NOI18N
        btnNuevoli.setText("Nuevo");
        btnNuevoli.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(102, 0, 102)));
        btnNuevoli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoliActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevoli);
        btnNuevoli.setBounds(260, 70, 80, 26);

        jPanel3.setBackground(new java.awt.Color(255, 255, 102));
        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 50, 30, 490);

        jPanel5.setBackground(new java.awt.Color(255, 255, 102));
        getContentPane().add(jPanel5);
        jPanel5.setBounds(20, 390, 330, 150);
        getContentPane().add(txtClave);
        txtClave.setBounds(30, 90, 112, 22);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoliActionPerformed
    this.btnAgregarLi1.setEnabled(true);
    this.txtNombre.setEnabled(true);
    this.txtNumMat.setEnabled(true);
    this.txtNumSem.setEnabled(true);
    this.txtClave.setEnabled(true);
    
    this.btnBuscarli.setEnabled(false);
    this.btnCancelarLi.setEnabled(true);
    }//GEN-LAST:event_btnNuevoliActionPerformed
public void Limpiar(){
        this.txtClave.setText("");
        this.txtNombre.setText("");
        this.txtNumMat.setText("");
        this.txtNumSem.setText("");
        
    }
    public void deshabilitar(){
    this.txtNombre.setEnabled(false);
    this.txtNumMat.setEnabled(false);
    this.txtNumSem.setEnabled(false);
    
    this.btnCancelarLi.setEnabled(false);
    this.btnAgregarLi1.setEnabled(false);
    this.btnBuscarli.setEnabled(true);
    
    }
    private void btnBuscarliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarliActionPerformed
    if (this.txtClave.equals("")){
    JOptionPane.showMessageDialog(this, "Falto capturar numero recibo");
    this.txtClave.requestFocus();
     }else{
        this.txtNombre.setEnabled(true);
        this.txtNumMat.setEnabled(true);
        this.txtNumSem.setEnabled(true);
        this.btnCancelarLi.setEnabled(true);
        this.btnAgregarLi1.setEnabled(true);
    dbCarreras db = new dbCarreras();
    carreras carr = new carreras();
    int clave= Integer.parseInt(this.txtClave.getText());
    carr = db.buscar(clave);
    if(carr.getIdCarrera()>0){
        this.txtNombre.setText(carr.getDescripcion());
        this.txtNumMat.setText(String.valueOf(carr.getNumMaterias()));
        this.txtNumSem.setText(String.valueOf(carr.getSemestres()));


    }else{
        JOptionPane.showMessageDialog(this,"No existe la clave numero " + this.txtClave.getText());
    }
} 
    
    }//GEN-LAST:event_btnBuscarliActionPerformed

    private void btnLimpiarliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarliActionPerformed
this.Limpiar();        // TODO add your handling code here:
    }//GEN-LAST:event_btnLimpiarliActionPerformed

    private void btnCancelarLiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarLiActionPerformed
this.btnBuscarli.setEnabled(true);
this.txtClave.setEnabled(true);
        this.Limpiar();
        this.deshabilitar();        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarLiActionPerformed

    private void btnVolverALiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverALiActionPerformed
                   int res = javax.swing.JOptionPane.showConfirmDialog(this, "¿Desea cerrar la ventana?","Carreras",javax.swing.JOptionPane.YES_NO_OPTION);

        if (res == javax.swing.JOptionPane.YES_OPTION) this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVolverALiActionPerformed

    private void btnAgregarLi1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarLi1ActionPerformed
if (this.txtNombre.getText().matches("") ||
    this.txtNumMat.getText().matches("") ||
    this.txtNumSem.getText().matches("")) {

    JOptionPane.showMessageDialog(this, "Falto capturar la información");

} else {
    carr.setClave(Integer.parseInt(this.txtClave.getText()));
    carr.setNumMaterias(Integer.parseInt(this.txtNumMat.getText()));
    carr.setSemestres(Integer.parseInt(this.txtNumSem.getText()));
    carr.setDescripcion(this.txtNombre.getText());

    if (db.isExiste(carr.getClave(), 0)) {
        this.isActualizar = true;
    }

    if (isActualizar == false) {
        if (!db.isExiste(carr.getClave(), 0)) {
            db.insertar(carr);
            JOptionPane.showMessageDialog(this, "Se agregó con éxito");

        } else {
            JOptionPane.showMessageDialog(this, "Ese número ya existe");
        }
    } else {
        db.actualizar(carr);
        JOptionPane.showMessageDialog(this, "Se actualizó con éxito");
        // TODO add your handling code here (if needed)
    }
}
       this.Limpiar();
        this.deshabilitar();  
    }//GEN-LAST:event_btnAgregarLi1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
                java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlg_Carreras dialog = new dlg_Carreras(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });

    }
private Boolean isActualizar = false;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAgregarLi1;
    public javax.swing.JButton btnBuscarli;
    public javax.swing.JButton btnCancelarLi;
    public javax.swing.JButton btnLimpiarli;
    public javax.swing.JButton btnNuevoli;
    public javax.swing.JButton btnVolverALi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    public javax.swing.JTextField txtClave;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtNumMat;
    public javax.swing.JTextField txtNumSem;
    // End of variables declaration//GEN-END:variables
private carreras carr = new carreras();
private dbCarreras db = new dbCarreras();
}
