/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Puesto;

/**
 *
 * @author Alain
 */
public class Puesto {
    //atributos de la clase
    private int id;
    private int numPuesto;
    private String descripcion;
    private int status;
    //constructores
    public Puesto(){
        this.id=0;
        this.numPuesto=0;
        this.descripcion="";
        this.status=0;
    }

    public Puesto(int id, int numPuesto, String descripcion, int status) {
        this.id = id;
        this.numPuesto = numPuesto;
        this.descripcion = descripcion;
        this.status = status;
    }
    
    public Puesto(Puesto otro){
        this.id = otro.id;
        this.numPuesto = otro.numPuesto;
        this.descripcion = otro.descripcion;
        this.status = otro.status;
    }
    
    //metodos
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumPuesto() {
        return numPuesto;
    }

    public void setNumPuesto(int numPuesto) {
        this.numPuesto = numPuesto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
}
