/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Puesto;

import java.sql.*;

/**
 *
 * @author Alain
 */
public class dbPuesto {
    
    private String MYSQLDRIVER = "com.mysql.cj.jdbc.Driver";
    private String MYSQLDB = "jdbc:mysql://3.132.136.208:3306/biblioteca?user=jesush&password=C0ntras3n1a";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    
    public dbPuesto(){
     try{
        Class.forName(MYSQLDRIVER);
     }catch(ClassNotFoundException e){
         System.out.println("Surgio un error " + e.getMessage());
         System.exit(-1);
     }
    }
    
    public void conectar(){
        try{
            conexion = DriverManager.getConnection(MYSQLDB);
        }catch (SQLException e){
            System.out.println("No se logro conectar " +e.getMessage());
        }
    }
    
    public void desconectar(){
        try{
            conexion.close();
        }catch (SQLException e){
            System.out.println("Surgio un error al desconectar" +e.getMessage());
        }
    }
    
    public void insertar(Puesto puesto){
        conectar();
        try{
            strConsulta = "INSERT INTO puesto(numpuesto,descripcion,status)"+"VALUES(?,?,?)";
            
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, puesto.getNumPuesto());
            pst.setString(2, puesto.getDescripcion());
            pst.setInt(3, puesto.getStatus());
            
            pst.executeUpdate();
        }catch (SQLException e){
            System.out.println("Error al insertar: " +e.getMessage());
        }
        desconectar();
    }
    
    
    public void actualizar(Puesto puesto){
        Puesto puesto2 = new Puesto();
        
        strConsulta = "UPDATE puesto SET descripcion = ? WHERE numpuesto = ? and status = 0;";
        this.conectar();
        try{
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
           
            pst.setString(1, puesto.getDescripcion());
            pst.setInt(2, puesto.getNumPuesto());
            
            pst.executeUpdate();
            this.desconectar();
        }catch(SQLException e){
            System.err.println("Surgio un error al actualizar: "+e.getMessage());
        }
    }
    
    public void habilitar(Puesto puesto){
        
        String consulta = "";
        strConsulta = "UPDATE puesto SET status = 0 WHERE numpuesto = ?";
        this.conectar();
        try{
            System.err.println("Se conecto");
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            
            pst.setInt(1, puesto.getNumPuesto());
            
            pst.executeUpdate();
            this.desconectar();
        }catch(SQLException e){
            System.err.println("Surgio un error al habilitar" +e.getMessage());
        }
    }
    
    public void deshabilitar(Puesto puesto){
        strConsulta = "UPDATE puesto SET status = 1 WHERE numpuesto = ?";
        this.conectar();
        try{
            System.err.println("Se conecto");
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            
            pst.setInt(1, puesto.getNumPuesto());
            
            pst.executeUpdate();
            this.desconectar();
        }catch(SQLException e){
            System.err.println("Surgio un error al habilitar "+e.getMessage());
        }  
    }
    
    public boolean isExiste(int numPuesto, int status){
        boolean exito=false;
        this.conectar();
        strConsulta="SELECT * FROM puesto WHERE numpuesto = ? and status = ?;";
        try{
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, numPuesto);
            pst.setInt(2, status);
            this.registros = pst.executeQuery();
            if(this.registros.next()) exito=true;
        }catch(SQLException e){
            System.err.println("Surgio un error al verificar si existe: "+e.getMessage());
        }
        this.desconectar();
        return exito;
    }
    
    public Puesto buscar(int numPuesto){
        Puesto puesto2=new Puesto();
        conectar();
        try{
            strConsulta= "SELECT * FROM puesto WHERE numpuesto = ? and status = 0;";
            PreparedStatement pst=conexion.prepareStatement(strConsulta);
            
            pst.setInt(1, numPuesto);
            this.registros=pst.executeQuery();
            if(this.registros.next()){
                puesto2.setId(registros.getInt("idpuesto"));
                puesto2.setNumPuesto(registros.getInt("numpuesto"));
                puesto2.setDescripcion(registros.getString("descripcion"));
            }else puesto2.setId(0);
        }catch(SQLException e){
            System.err.println("Surgio un error al habilitar: "+e.getMessage());
        }
        this.desconectar();
        return puesto2;
    }
    
    
    
}
