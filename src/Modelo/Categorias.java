package Modelo;

/**
 *
 * @author LGCD
 */
public class Categorias {
    //Atributos de la clase
    private int id;
    private int codigo;
    private String descripcion;
    private int status;

    public Categorias() {
        this.id=0;
        this.codigo=0;
        this.descripcion="";
        this.status=0;
    }

    public Categorias(int id, int codigo, String descripcion, int status) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.status = status;
    }
    
    public Categorias(Categorias otro) {
        this.id = otro.id;
        this.codigo = otro.codigo;
        this.descripcion = otro.descripcion;
        this.status = otro.status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
    
}
