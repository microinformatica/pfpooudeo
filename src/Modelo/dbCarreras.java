/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package carreras;

import java.sql.*;
/**
 *
 * @author Alexis
 */
public class dbCarreras {
        private static final String MYSQLDRIVER = "com.mysql.cj.jdbc.Driver";
        private static final String MYSQLDB = "jdbc:mysql://3.132.136.208:3306/biblioteca?user=alxsddd&password=uwu14252";
        private Connection conexion;
        private String strConsulta;
        private ResultSet registros;
        
        public dbCarreras(){
    try {
            Class.forName(MYSQLDRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Surgió un error " + e.getMessage());
            System.exit(-1);
        }try {
            Class.forName(MYSQLDRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Surgió un error " + e.getMessage());
            System.exit(-1);
        }
}
         public void conectar() {
        try {
            conexion = DriverManager.getConnection(MYSQLDB);
        } catch (SQLException e) {
            System.out.println("No se logró conectar " + e.getMessage());
        }
    }
        public void desconectar() {
        try {
            if (conexion != null && !conexion.isClosed()) {
                conexion.close();
            }
        } catch (SQLException e) {
            System.out.println("Surgió un error al desconectar " + e.getMessage());
        }
    }
        public void insertar(carreras car) {
        conectar();
        try {
            strConsulta = "INSERT INTO carreras(clave, descripcion, semestres, nummaterias, status) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setInt(1, car.getClave());
            pst.setString(2, car.getDescripcion());
            pst.setInt(3, car.getSemestres());
            pst.setInt(4, car.getNumMaterias());
            pst.setInt(5, car.getStatus());

            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error al insertar " + e.getMessage());
        } finally {
            desconectar();
        }
    }
        
           public void actualizar(carreras car) {
        strConsulta = "UPDATE carreras SET descripcion=?, semestres=?, nummaterias=?  WHERE clave=? and status=0";
        conectar();
        try {
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setString(1, car.getDescripcion());
            pst.setInt(2, car.getSemestres());
            pst.setInt(3, car.getNumMaterias());
            pst.setInt(4, car.getClave());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Surgió un error al actualizar: " + e.getMessage());
        }
    }
           
               public boolean isExiste(int clave, int status) {
        boolean exito = false;
        strConsulta = "SELECT * FROM carreras WHERE clave = ? AND status = ?";
        try {
            conectar();
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, clave);
            pst.setInt(2, status);
            this.registros = pst.executeQuery();
            if (this.registros.next()) {
                exito = true;
            }
        } catch (SQLException e) {
            System.err.println("Surgió un error al verificar si existe: " + e.getMessage());
        } finally {
            desconectar();
        }
        return exito;
    }
                   public carreras buscar(int clave) {
        carreras carr = new carreras();
        conectar();
        try {
            strConsulta = "SELECT * FROM carreras WHERE clave = ? AND status = 0";
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, clave);
            this.registros = pst.executeQuery();
            if (this.registros.next()) {
                carr.setIdCarrera(registros.getInt("idcarrera"));
                carr.setClave(registros.getInt("clave"));
                carr.setDescripcion(registros.getString("descripcion"));
                carr.setSemestres(registros.getInt("semestres"));
                carr.setNumMaterias(registros.getInt("nummaterias"));

            } else {
                carr.setIdCarrera(0);
            }
        } catch (SQLException e) {
            System.err.println("Surgió un error al buscar " + e.getMessage());
        } finally {
            desconectar();
        }
        return carr;
    }
}
