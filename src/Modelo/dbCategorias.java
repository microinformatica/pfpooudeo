package Modelo;

import java.sql.*;

/**
 *
 * @author LGCD
 */
public class dbCategorias {
    
    private String MYSQLDRIVER = "com.mysql.cj.jdbc.Driver";
    private String MYSQLDB = "jdbc:mysql://3.132.136.208:3306/biblioteca?user=cguerra&password=recovery2010";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    //constructor
    public dbCategorias() {
    
        try {
            Class.forName(MYSQLDRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Surgio un error "+e.getMessage());
            System.exit(-1);
        }
    }
    
    public void conectar() {
    
        try {
            conexion = DriverManager.getConnection(MYSQLDB);
        } catch (SQLException e) {
            System.out.println("No se logró conectar "+e.getMessage());
        }
    }
    
    public void desconectar() {
    
        try {
            conexion.close();
        } catch (SQLException e) {
            System.out.println("Surgió un error al desconectar "
                    +e.getMessage());
        }
    }
    
    public void insertar(Categorias cate) {
    conectar();
        try{
        
            strConsulta = "INSERT INTO categorias(codigo,descripcion,status)"
                    +"VALUES(?,?,?)";
            
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, cate.getCodigo());
            pst.setString(2, cate.getDescripcion());
            pst.setInt(3, cate.getStatus());
            
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error al insertar "+e.getMessage());
        }
        desconectar();
    }
    
    public void actualizar(Categorias cate) {
    Categorias categorias = new Categorias();
    
    strConsulta="UPDATE categorias SET descripcion = ? WHERE codigo = ? and status = 0;";
    this.conectar();
    try {
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
         //Asignar los valores a la consulta  
            pst.setString(1, cate.getDescripcion());
            pst.setInt(2, cate.getCodigo());
    
            pst.executeUpdate();
            this.desconectar();
        } catch (SQLException e) {
            System.out.println("Surgio un error al actualizar: "+e.getMessage());
        }
    }
    
    public void habilitar(Categorias cate) {
    
        String consulta = "";
        strConsulta = "UPDATE categorias SET status = 0 WHERE codigo = ?";
        this.conectar();
        try {
            System.err.println("Se conecto");
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            //Asignar los valores a la consulta
            pst.setInt(1, cate.getCodigo());
            
            pst.executeUpdate();
            this.desconectar();
        } catch (SQLException e) {
            System.err.println("Surgio un error al habilitar: "+e.getMessage());
        }
    }
    
    public void deshabilitar(Categorias cate) {
    strConsulta = "UPDATE categorias SET status = 1 WHERE codigo = ?";
    this.conectar();
    try {
        System.err.println("Se conectó");
        PreparedStatement pst = conexion.prepareStatement(strConsulta);
        //Asignar los valores a la consulta
        pst.setInt(1, cate.getCodigo());
            
            pst.executeUpdate();
            this.desconectar();
            
        } catch (SQLException e) {
            System.err.println("Surgio un error al habilitar: "+e.getMessage());
    }
    }
    
    public boolean isExiste(int codigo, int status) {
        boolean exito = false;
        this.conectar();
        strConsulta = "SELECT * FROM categorias WHERE codigo = ? and status = ?;";
        try {
        PreparedStatement pst = conexion.prepareStatement(strConsulta);
        pst.setInt(1, codigo);
        pst.setInt(2, status);
        this.registros = pst.executeQuery();
        if(this.registros.next()) exito = true;
        
        } catch(SQLException e ){
            System.err.println("Surgió un error al verificar si existe: "+e.getMessage());
        }
        this.desconectar();
        return exito;
    }
    
    public Categorias buscar(int codigo){
    Categorias categorias = new Categorias();
    conectar();
    try {
        strConsulta = "SELECT * FROM categorias WHERE codigo = ? and status = 0;";
        PreparedStatement pst = conexion.prepareStatement(strConsulta);
        
        pst.setInt(1, codigo);
        this.registros = pst.executeQuery();
        if(this.registros.next()) {
        
            categorias.setId(registros.getInt("idcategoria"));
            categorias.setCodigo(registros.getInt("codigo"));
            categorias.setDescripcion(registros.getString("descripcion"));

        } else categorias.setId(0);
    } catch(SQLException e){
        System.err.println("Surgió un eror al habilitar: "+e.getMessage());
    }
    this.desconectar();
    return categorias;
    }
}



