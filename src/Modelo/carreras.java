/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package carreras;

/**
 *
 * @author Alexis
 */
public class carreras {
    
    private int idCarrera;
    private int clave;
    private String descripcion;
    private int semestres;
    private int numMaterias;
    private int status;
    
   public carreras(){
       
       this.idCarrera = 0;
       this.clave = 0;
       this.descripcion = "";
       this.semestres = 0;
       this.numMaterias = 0;
       this.status = 0;
   }
            
         public carreras(int idCarreras, int clave, String descripcion, int semestres, int numMaterias, int status){
         
             this.idCarrera = idCarrera;
             this.clave = clave;
             this.descripcion = descripcion;
             this.semestres = semestres;
             this.numMaterias = numMaterias;
             this.status = status;
         }   
         
         public carreras(carreras otro){
             this.idCarrera = idCarrera;
             this.clave = otro.clave;
             this.descripcion = otro.descripcion;
             this.semestres = otro.semestres;
             this.numMaterias = otro.numMaterias;
             this.status = otro.status;
         }

    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getSemestres() {
        return semestres;
    }

    public void setSemestres(int semestres) {
        this.semestres = semestres;
    }

    public int getNumMaterias() {
        return numMaterias;
    }

    public void setNumMaterias(int numMaterias) {
        this.numMaterias = numMaterias;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
         
         
}
